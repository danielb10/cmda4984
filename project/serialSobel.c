#include <png.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int width, height;
png_byte color_type;
png_byte bit_depth;

png_structp png_ptr;
png_infop info_ptr;
png_bytep* row_pointers;

void read_png(char* filename){
  
  char header[8];
  FILE *fp = fopen(filename, "rb");
  fread(header, 1, 8, fp);
  png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  info_ptr = png_create_info_struct(png_ptr);


  png_init_io(png_ptr, fp);
  png_set_sig_bytes(png_ptr, 8);
  
  png_read_info(png_ptr, info_ptr);


  width = png_get_image_width(png_ptr, info_ptr);
  height = png_get_image_height(png_ptr, info_ptr);
  bit_depth = png_get_bit_depth(png_ptr, info_ptr);
  color_type = png_get_color_type(png_ptr, info_ptr);
 
  png_read_update_info(png_ptr, info_ptr);

  row_pointers = (png_bytep*) malloc(sizeof(png_bytep) * height);
  for (int i=0;i<height;i++){
    row_pointers[i] = (png_byte*) malloc(png_get_rowbytes(png_ptr, info_ptr));
  }

  png_read_image(png_ptr, row_pointers);

  fclose(fp);

}


void write_png(char* filename){
  FILE* fp = fopen(filename, "wb");

  png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  info_ptr = png_create_info_struct(png_ptr);
  png_init_io(png_ptr, fp);


  png_set_IHDR(png_ptr, info_ptr, width, height, bit_depth, color_type, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

  png_write_info(png_ptr, info_ptr);

  png_write_image(png_ptr, row_pointers);


  png_write_end(png_ptr, NULL);

  free(row_pointers);
  fclose(fp);

}

int change_to_gray(){
  for (int i=0;i<height;i++){
    png_byte* row = row_pointers[i];
    for (int j=0; j<width; j++){
      png_byte* ptr = &(row[j*3]);
      int gray = (ptr[0] + ptr[1] + ptr[2])/3;
      ptr[1] = gray;
      ptr[2] = gray;
      ptr[3] = gray;

    }
  }
  return 0;
}

int convolution(int offset, int kernel[3][3], int row,
    png_bytep* row_pointers) {
  int sum = 0;
  for (int i = 0; i < 3; i++) {
    png_byte* current_row = row_pointers[i+row-1];
    for (int j = 0; j < 3; j++) {
      sum += current_row[offset*3 + j*3 -3] * kernel[i][j];
    } 
  }

  return sum;
}


int sobel(){

  int Gx[3][3] = { {-1,0,1},{-2,0,2},{-1,0,1} };
  int Gy[3][3] = { {-1,-2,-1},{0,0,0},{1,2,1} };
  int* new_vals = (int*) malloc(sizeof(int)*(png_get_rowbytes(png_ptr, info_ptr)*4/3) * height);
 

  for (int i=1;i<height-1;i++){
    for (int j=1;j<width-1;j++){
        int xconv = convolution(j, Gx, i, row_pointers);
        int yconv = convolution(j, Gy, i, row_pointers);
        new_vals[(i-1)*(width) + j-1] = sqrt(xconv*xconv + yconv*yconv);       
    } 
  }  

  
  for (int i=1;i<height-1;i++){
    png_byte* row = row_pointers[i-1];
    for (int j=1;j<width-1;j++){
        png_byte* ptr = &(row[j*3]);
        ptr[0] = new_vals[((i-1)*width+j-1)];
        ptr[1] = new_vals[((i-1)*width+j-1)];
        ptr[2] = new_vals[((i-1)*width+j-1)];
    } 
  }  

  return 0;
}


int main(int argc, char **argv){
  //printf("width: %d height: %d\n", width, height);  
  
  // Read PNG
  read_png(argv[1]);

  // Change to grayscale
  change_to_gray();

  // Perform Sobel edge detection
  sobel();

  // Save new image
  write_png(argv[2]);

  return 0;  

}
