Run in parallel:
./sobel {target image} {new image name}

Run in serial:
./serialSobel {target image} {new image name}

