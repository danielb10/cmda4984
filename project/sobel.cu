#include <png.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int width, height;
png_byte color_type;
png_byte bit_depth;

png_structp png_ptr;
png_infop info_ptr;
png_bytep* row_pointers;

void read_png(char* filename){
  
  //char header[8];
  FILE *fp = fopen(filename, "rb");
  //fread(header, 1, 8, fp);
  png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  info_ptr = png_create_info_struct(png_ptr);

  png_jmpbuf(png_ptr);

  png_init_io(png_ptr, fp);
  png_set_sig_bytes(png_ptr, 0);
  
  png_read_info(png_ptr, info_ptr);


  width = png_get_image_width(png_ptr, info_ptr);
  height = png_get_image_height(png_ptr, info_ptr);
  color_type = png_get_color_type(png_ptr, info_ptr);
  bit_depth = png_get_bit_depth(png_ptr, info_ptr);

  png_read_update_info(png_ptr, info_ptr);
  setjmp(png_jmpbuf(png_ptr));

  row_pointers = (png_bytep*) malloc(sizeof(png_bytep) * height);
  for (int i=0;i<height;i++){
    row_pointers[i] = (png_byte*) malloc(png_get_rowbytes(png_ptr, info_ptr));
  }
  png_read_image(png_ptr, row_pointers);

 

  fclose(fp);
}


void write_png(char* filename){
  FILE* fp = fopen(filename, "wb");

  png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  info_ptr = png_create_info_struct(png_ptr);
  setjmp(png_jmpbuf(png_ptr));
  png_init_io(png_ptr, fp);

  //setjmp(png_jmpbuf(png_ptr));

  png_set_IHDR(png_ptr, info_ptr, width, height, bit_depth, color_type, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

  png_write_info(png_ptr, info_ptr);

  //setjmp(png_jmpbuf(png_ptr));

  png_write_image(png_ptr, row_pointers);

  setjmp(png_jmpbuf(png_ptr));

  png_write_end(png_ptr, NULL);

  free(row_pointers);
  fclose(fp);

}

int change_pixels(){
  for (int i=0;i<height;i++){
    png_byte* row = row_pointers[i];
    for (int j=0; j<width; j++){
      png_byte* ptr = &(row[j*3]);
      int gray = (ptr[0] + ptr[1] + ptr[2])/3;
      ptr[1] = gray;
      ptr[2] = gray;
      ptr[3] = gray;

    }
  }
  return 0;
}

__device__ int convolution(int offset, int kernel[3][3],
    int* pic, int width) {
  int sum = 0;
  
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      sum += *(pic + (offset+((i-1)*width*3) + j*3 - 3)) * kernel[i][j];
    } 
  }

  return sum;
}


__global__ void sobelKernel(int width, int height, int* pic, int*
    new_vals){

  int i = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;
  int idx = B * b + i; 
  

  
  if (idx >= height - 2 || idx == 0){
    return;
  }
  
  int Gx[3][3] = { {-1,0,1},{-2,0,2},{-1,0,1} };
  int Gy[3][3] = { {-1,-2,-1},{0,0,0},{1,2,1} };

  
  for (int j=1;j<width;j++){
    int xconv = convolution(idx*width*3+j*3, Gx, pic, width);
    int yconv = convolution(idx*width*3+j*3, Gy, pic, width);
    new_vals[(idx-1)*width + j-1] = sqrt((float)(xconv*xconv + yconv*yconv));

  }


  int* row = &pic[idx*width*3];
  for (int j=1;j<width-1;j++){
     
    row[j*3]   = new_vals[((idx-1)*width+j-1)];
    row[j*3+1] = new_vals[((idx-1)*width+j-1)];
    row[j*3+2] = new_vals[((idx-1)*width+j-1)];
  }
  
}

__global__ void blurKernel(int width, int height, int* pic, int*
    new_vals){

  int i = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;
  int idx = B * b + i; 
  

  
  if (idx >= height - 2 || idx == 0){
    return;
  }
  
  int blur[3][3] = { {1,1,1},{1,1,1},{1,1,1} };

   for (int j=1;j<width;j++){
    int blurVal = convolution(idx*width*3+j*3, blur, pic, width);
    new_vals[(idx-1)*width + j-1] = blurVal/9;

  }

  int* row = &pic[idx*width*3];
  for (int j=1;j<width-1;j++){
     
    row[j*3]   = new_vals[((idx-1)*width+j-1)];
    row[j*3+1] = new_vals[((idx-1)*width+j-1)];
    row[j*3+2] = new_vals[((idx-1)*width+j-1)];
  }
 
}


__global__ void sobelKernelv2(int width, int height, int* pic, int*
    new_vals){

  int i = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;
  int idx = B * b + i;
  
  
  if (b >= height - 1 || b == 0 || i == 0 || i >= width - 2){
    return;
  }


  int Gx[3][3] = { {-1,0,1},{-2,0,2},{-1,0,1} };
  int Gy[3][3] = { {-1,-2,-1},{0,0,0},{1,2,1} };


  int xconv = convolution(idx*3, Gx, pic, width);
  int yconv = convolution(idx*3, Gy, pic, width);
  new_vals[idx - width - 1] = sqrt((float)(xconv*xconv + yconv*yconv));
  
  
  pic[idx*3]   = new_vals[idx - width - 1];
  pic[idx*3+1] = new_vals[idx - width - 1];
  pic[idx*3+2] = new_vals[idx - width - 1];
  
}

int main(int argc, char **argv){
  // Read PNG
  read_png(argv[1]);

  printf("width: %d height: %d\n", width, height);  
  // Change to grayscale
  change_pixels();



  int tmp_pic[height*width*3];

  for (int i=0;i<height;i++){
    for (int j=0;j<width*3;j++){
      tmp_pic[i*width*3+j] = row_pointers[i][j];
    }
  }
  
  
 
  int* pic;
  int* new_vals;
  
  cudaMalloc(&pic, height*width*sizeof(int)*3);
  cudaMalloc(&new_vals, height*width*sizeof(int));

  
  cudaMemcpy(pic, tmp_pic, 4*height*width*3, cudaMemcpyHostToDevice);

  
  
  
  //blurKernel<<<width, height>>>(width, height, pic, new_vals);
  
  sobelKernel<<<width, height>>>(width, height, pic, new_vals);
  
  //sobelKernelv2<<<height, width>>>(width, height, pic, new_vals);
  
  cudaDeviceSynchronize();

  cudaMemcpy(tmp_pic, pic, 4*height*width*3, cudaMemcpyDeviceToHost);
  
  for (int i=0;i<height;i++){
    for (int j=0;j<width*3;j++){
      row_pointers[i][j] = (png_byte) tmp_pic[i*width*3+j];
    }
  }
  
  
  // Save new image
  write_png(argv[2]);

  return 0;  

}
