#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "cuda.h"


// Fill each array
__global__ void fillKernel(int N, float val, float *c_x, float *c_y, float*c_z) {

  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;

  int n = t + b*B;

  if(n<N) {
    c_x[n] = 2*n + val;
    c_y[n] = val;
  }


  // Set to 0 as a placeholder
  c_z[n] = 0;
}



// Divide values in the x array by the values in the y array
__global__ void divisionKernel(float *c_z, float *c_x, float* c_y) {
  
  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;

  int n = t + b*B;

  c_z[n] = c_x[n] / c_y[n];
}


// Takes the tangent of every value in the array
__global__ void tangentKernel(float *c_z) {

  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;

  int n = t + b*B;
  
  c_z[n] = tan(c_z[n]);
}

// Finds e^x for each value in the array
__global__ void exponentialKernel(float *c_z) {

  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;

  int n = t + b*B;

  c_z[n] = exp(c_z[n]);
}



// Create functions to perform operations on cpu



// Initializes arrays
int fillHost(int N, float val, float *h_z, float *h_x, float *h_y) {
  
  for (int i=0;i<N;i++){
    h_x[i] = 2.0*i + val;
    h_y[i] = val;
    h_z[i] = 0;
  }

  return 0;
}


// Performs vector division on the host
int divisionHost(int N, float *h_z, float *h_x, float *h_y) {
  
  for (int i=0;i<N;i++){
    h_z[i] = h_x[i] / h_y[i];
  }

  return 0;
}

// Takes the tangent of every value in the array
int tangentHost(int N, float *h_z) {
  
  for (int i=0;i<N;i++){
    h_z[i] = tan(h_z[i]);
  }

  return 0;
}


// Finds e^x for every value in the array
int exponentialHost(int N, float *h_z){

  for (int i=0;i<N;i++){
    h_z[i] = exp(h_z[i]);
  }

  return 0;
}


// Finds the l1 norm for two vectors
int norm(float *l1, int N, float *h_z, float *c_z){
  
  for (int i=0;i<N;i++){
    *l1 += fabs(h_z[i] - c_z[i]);
  }
  
  return 0;
}




// Host main function
int main(int argc, char **argv){

  if(argc<2){
    printf("Usage: ./cudaFillVector N\n");
    exit(-1);
  }


  // Set device
  cudaSetDevice(0);

  // read command line arg
  int N = atoi(argv[1]);

  // allocate HOST arrays
  float *h_z = (float*) malloc(N*sizeof(float));
  float *h_x = (float*) malloc(N*sizeof(float));
  float *h_y = (float*) malloc(N*sizeof(float));
  float val = 1.5;

  // Perform calculations on the host
  fillHost(N, val, h_z, h_x, h_y);
  divisionHost(N, h_z, h_x, h_y);
  tangentHost(N, h_z);
  exponentialHost(N, h_z);
  

  // allocate device arrays
  float *c_x;
  float *c_y;
  float *c_z;

  cudaMalloc(&c_x, N*sizeof(float));
  cudaMalloc(&c_y, N*sizeof(float));
  cudaMalloc(&c_z, N*sizeof(float));


  // GPU calculations
  

  int B = 256;
  int G = (N+B-1)/B;

  // Run other kernels
  fillKernel <<< G, B >>> (N, val, c_x, c_y, c_z);
  divisionKernel <<< G, B >>> (c_z, c_x, c_y);
  tangentKernel <<< G, B >>> (c_z);
  exponentialKernel <<< G, B >>> (c_z);



  // Allocate array on host to copy data from device to
  float *device_z = (float*) malloc(N*sizeof(float));

  // copy data from device outcome array to host
  cudaMemcpy(device_z, c_z, N*sizeof(float), cudaMemcpyDeviceToHost);



  // Compute the l1 norm of the two vectors
  float l1 = 0;

  norm(&l1, N, h_z, device_z);

  // Print result
  for (int i=0;i<N;i++){
    printf("device[%d] = %g\n", i, device_z[i]);
    printf("host[%d]   = %g\n", i, h_z[i]);
  }
  
  printf("L1 norm: %f\n", l1);

  return 0;
}



