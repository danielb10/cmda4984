#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "cuda.h"


// Fill each array
__global__ void fillKernel(int N, float val, float *c_x, float *c_y, float*c_z) {

  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;

  int n = t + b*B;

  if(n<N) {
    c_x[n] = 2*n + val;
    c_y[n] = val;
    c_z[n] = 0;
  }


  // Set to 0 as a placeholder
  // c_z[n] = 0;
}



// Divide values in the x array by the values in the y array
__global__ void divisionKernel(int N, float *c_z, float *c_x, float* c_y) {
  
  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;

  int n = t + b*B;
  if(n<N) {     
    c_z[n] = c_x[n] / c_y[n];
  }
}


// Takes the tangent of every value in the array
__global__ void tangentKernel(int N, float *c_z) {

  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;

  int n = t + b*B;
  if (n<N) {
    c_z[n] = tan(c_z[n]);
  }
}

// Finds e^x for each value in the array
__global__ void exponentialKernel(int N, float *c_z) {

  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;

  int n = t + b*B;
  
  if (n<N){
    c_z[n] = exp(c_z[n]);
  }
}


void linearLeastSquaresFit(int M, double *B, double *T, double *T0, double
    *Winf) {
  
  double A00 = M;
  double A01 = 0;
  double A11 = 0;
  double b0 = 0;
  double b1 = 0;

  for(int m=0;m<M;m++){
    A01 += B[m];
    A11 += B[m]*B[m];
    b0  += T[m];
    b1  += B[m]*T[m];
  }

  double A10 = A01;

  double J = A00*A11 - A01*A10;
  double invA00 = A11/J;
  double invA01 = -A01/J;
  double invA10 = -A10/J;
  double invA11 = A00/J;

  *T0 = invA00*b0 + invA01*b1;
  *Winf = invA10*b0 + invA11*b1;



} 

    
// Host main function
int main(int argc, char **argv){

  // Set device
  cudaSetDevice(0);

  // read command line arg
  // int N = atoi(argv[1]);
  int N = 1e8;
 
  float val = 1.5;

  // allocate device arrays
  float *c_x;
  float *c_y;
  float *c_z;

 // cudaMalloc(&c_x, N*sizeof(float));
 // cudaMalloc(&c_y, N*sizeof(float));
 // cudaMalloc(&c_z, N*sizeof(float));


  // GPU calculations
  

  int B = 256;
  int G = (N+B-1)/B;

  // Create cuda timers
  //cudaEvent_t start, end;

  //cudaEventCreate(&start);
  //cudaEventCreate(&end);  
  

  // Insert timer start into queue
  // cudaEventRecord(start);
  
  // 

  int count = 0;
  for (int n=1;n<=N;n*=10){
    ++count;
  }
  
  double *d_recordEventDataSize = (double*) calloc(count, sizeof(double));
  double *t_recordEventDataSize = (double*) calloc(count, sizeof(double));
  
  //Create separate time arrays for each kernel
  double *d_recordEventTimes = (double*) calloc(count, sizeof(double));
  
  double *t_recordEventTimes = (double*) calloc(count, sizeof(double));
  
  double *e_recordEventTimes = (double*) calloc(count, sizeof(double));


  count = 0;
  for (int n=1;n<=N;n*=10) {
    
    cudaMalloc(&c_x, n*sizeof(float));
    cudaMalloc(&c_y, n*sizeof(float));
    cudaMalloc(&c_z, n*sizeof(float));


    
    // Run fill kernel
    fillKernel <<< G, B >>> (n, val, c_x, c_y, c_z);

    // Create cuda events for timers
    cudaEvent_t d_start, d_end;

    cudaEventCreate(&d_start);
    cudaEventCreate(&d_end);  
  

    // Insert timer start into queue
    cudaEventRecord(d_start);
  
    // Run Division kernels
    divisionKernel <<< G, B >>> (n, c_z, c_x, c_y);
  
    cudaEventRecord(d_end);

    cudaDeviceSynchronize();

    // Find how much time passed
    float d_elapsedEvent;
    cudaEventElapsedTime(&d_elapsedEvent, d_start, d_end);
  
    // Change units to seconds
    d_elapsedEvent /= 1000;

    d_recordEventDataSize[count] = n*sizeof(float)*3;
    d_recordEventTimes[count] = d_elapsedEvent;


    // Tangent kernel timing


    // Create cuda timers
    cudaEvent_t t_start, t_end;

    cudaEventCreate(&t_start);
    cudaEventCreate(&t_end);  
  

    // Insert timer start into queue
    cudaEventRecord(t_start);
  
    // Compute tangent
    tangentKernel <<< G, B >>> (n, c_z);
  
    cudaEventRecord(t_end);

    cudaDeviceSynchronize();

    // Find how much time passed
    float t_elapsedEvent;
    cudaEventElapsedTime(&t_elapsedEvent, t_start, t_end);
  
    // Change units to seconds
    t_elapsedEvent /= 1000;
    t_recordEventDataSize[count] = n*sizeof(float);
    t_recordEventTimes[count] = t_elapsedEvent;


    // Exponential kernel timing


    // Create cuda events
    cudaEvent_t e_start, e_end;

    cudaEventCreate(&e_start);
    cudaEventCreate(&e_end);  
  

    // Insert start into queue
    cudaEventRecord(e_start);
  
    // Compute exponential
    exponentialKernel <<< G, B >>> (n, c_z);
  
    // Inserd end event
    cudaEventRecord(e_end);

    cudaDeviceSynchronize();

    // Find how much time passed
    float e_elapsedEvent;
    cudaEventElapsedTime(&e_elapsedEvent, e_start, e_end);
    
    // Change units to seconds
    e_elapsedEvent /= 1000;
    e_recordEventTimes[count] = e_elapsedEvent;


    count++;
  
  }
  

  double d_t0, d_Winf;
  linearLeastSquaresFit(count, d_recordEventDataSize, d_recordEventTimes,
      &d_t0, &d_Winf);

  d_Winf = 1./d_Winf;
  d_Winf /= 1.e9;
  printf("Division t0 = %g (s), Winf = %g (GB/s)\n", d_t0, d_Winf);
  
  double t_t0, t_Winf;
  linearLeastSquaresFit(count, t_recordEventDataSize, t_recordEventTimes,
      &t_t0, &t_Winf);

  t_Winf = 1./t_Winf;
  t_Winf /= 1.e9;
  printf("Tangent t0 = %g (s), Winf = %g (GB/s)\n", t_t0, t_Winf);
  
  double e_t0, e_Winf;
  linearLeastSquaresFit(count, t_recordEventDataSize, e_recordEventTimes,
      &e_t0, &e_Winf);

  e_Winf = 1./e_Winf;
  e_Winf /= 1.e9;
  printf("Exponential t0 = %g (s), Winf = %g (GB/s)\n", e_t0, e_Winf);
  

  // Create file to store data
  FILE* f;

  f = fopen("output.txt", "w+");

  if (f == NULL){
    printf("Error creating file");
    exit(1);
  }

  
  fprintf(f, "Time,Throughput,Size\n");
  for (int i=0;i<count;i++){
    fprintf(f, "%g, %g, %g\n", d_recordEventTimes[i],
       ((d_recordEventDataSize[i]/1e9)/d_recordEventTimes[i]),
        d_recordEventDataSize[i]/1e9);
  }


  fprintf(f, "\n\n");
  for (int i=0;i<count;i++){
    fprintf(f, "%g, %g, %g\n", t_recordEventTimes[i],
        ((t_recordEventDataSize[i]/1e9)/t_recordEventTimes[i]),
        t_recordEventDataSize[i]/1e9);
  }


  fprintf(f, "\n\n");
  for (int i=0;i<count;i++){
    fprintf(f, "%g, %g, %g\n", e_recordEventTimes[i],
        ((t_recordEventDataSize[i]/1e9)/e_recordEventTimes[i]),
        t_recordEventDataSize[i]/1e9);
  }
  fclose(f);


  cudaFree(c_z);
  cudaFree(c_y);
  cudaFree(c_x);

  return 0;
}



