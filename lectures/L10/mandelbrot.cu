/*

To compile:
gcc -O3  -o mandelbrot mandelbrot.c png_util.c -I. -lm -lpng

To run:
./mandelbrot

*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

extern "C"
{
  #include "png_util.h"
}
__global__ void mandelbrot(const int NRe, 
		const int NIm, 
		const float minRe,
		const float minIm,
		const float dRe, 
		const float dIm,
		float* h_count){


  int tx = threadIdx.x;
  int bx = blockIdx.x;
  int dx = blockDim.x;
  
  int ty = threadIdx.y;
  int by = blockIdx.y;
  int dy = blockDim.y;

  int column = tx + bx*dx;
  int row    = ty + by*dy;
  
  if(row < NRe && column < NIm){

    // assume column major
    int n = row*NIm + column;

  
  // replace m and n with grid coords
  float cRe = minRe + column*dRe;
  float cIm = minIm + row*dIm;

  float zRe = 0;
  float zIm = 0;
      
  int Nt = 200;
  int t, cnt=0;
  for(t=0;t<Nt;++t){
	
	  // z = z^2 + c
	  //   = (zRe + i*zIm)*(zRe + i*zIm) + (cRe + i*cIm)
	  //   = zRe^2 - zIm^2 + 2*i*zIm*zRe + cRe + i*cIm
	  float zReTmp = zRe*zRe - zIm*zIm + cRe;
	  zIm = 2.f*zIm*zRe + cIm;
	  zRe = zReTmp;

	  cnt += (zRe*zRe+zIm*zIm<4.f);
  }

      h_count[n] = cnt;
  }
}
  


int main(int argc, char **argv){

  const int NRe = 4096;
  const int NIm = 4096;

  /* box containing sample points */
  const float centRe = -.7598, centIm= .125547;
  const float diam  = 0.551579;
  const float minRe = centRe-1.5*diam;
  const float remax = centRe+1.5*diam;
  const float minIm = centIm-1.5*diam;
  const float immax = centIm+1.5*diam;

  const float dRe = (remax-minRe)/(NRe-1.f);
  const float dIm = (immax-minIm)/(NIm-1.f);

  float *h_count = (float*) calloc(NRe*NIm, sizeof(float));

  float *c_count;
  cudaMalloc(&c_count ,NRe*NIm*sizeof(float)); 

  cudaEvent_t start, end;

  cudaEventCreate(&start);
  cudaEventCreate(&end);

  cudaEventRecord(start);


  dim3 B(16,16,1);
  dim3 G((NRe+16-1)/16, (NIm+16-1)/16, 1);

  mandelbrot <<< G, B >>> (NRe, NIm, minRe, minIm, dRe, dIm, c_count);

  cudaEventRecord(end);

  float elapsedEvent;
  cudaEventElapsedTime(&elapsedEvent, start, end); 
  
  printf("elapsed time %g\n", elapsedEvent);

  cudaMemcpy(h_count, c_count, NRe*NIm*sizeof(float), cudaMemcpyDeviceToHost);

  FILE *png = fopen("mandelbrot3.png", "w");
  write_hot_png(png, NRe, NIm, h_count, 0, 80);
  fclose(png);

}


