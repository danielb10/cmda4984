T0 = 2e-6;      %% kernel overhead estimate
Rpeak = 360e9;  %% streaming DEVICE bw
Fpeak = 11e12;  %% high percentage of peak DEVICE GFLOPS / s]

%% grid of AI and B values
maxAI = 1.1* Fpeak / Rpeak ; %% maximum arithmetic intensity ( AI )
maxB = 2e9 ; %% maximum number of bytes
NAI = 500; %% number of AI samples
NB  = 500; %% number of bytes values to sample

AI = linspace (0 , maxAI , NAI )'* ones (1 , NB ) ;
B  = ones(NAI ,1)* 10.^ linspace (0 , 9 , NB ) ; %% up to 1 GB of data

%% model for execution time
TB = T0 + B .* max (1/ Rpeak , AI / Fpeak ) ;

%% model for throughput in GLOPS
GFLOPS = ( B .* AI ./ TB ) /1e9 ;

surf ( AI , GFLOPS , log10 ( B ) )
view(2);
shading interp
ha = colorbar

xlabel('Arithmetic intensity (A_I in FLOPS/BYTE)', 'fontsize', 20);
ylabel('Arithmetic throughput (GFLOPS/s) ', 'fontsize', 20);

title('log10(bytes) required to reach the roofline', 'fontsize', 20)



