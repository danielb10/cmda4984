
#include <math.h>
#include <stdio.h>
#include <cuda.h>

__global__ void matrixTransposeKernel(int Nrows, int Ncolumns, float *A, float *AT){

  int tx = threadIdx.x;
  int bx = blockIdx.x;
  int dx = blockDim.x;
  
  int ty = threadIdx.y;
  int by = blockIdx.y;
  int dy = blockDim.y;

  int column = tx + bx*dx;
  int row    = ty + by*dy;

  if(row < Nrows && column < Ncolumns){

    // assume column major
    int n = row*Ncolumns + column;

    float Arc = A[n];

    // switch to row major
    int nT = column*Nrows + row;
    AT[nT] = Arc;
  }
  
}

int main(int argc, char **argv){

  int Nrows = atoi(argv[1]);
  int Ncolumns = atoi(argv[2]);

  float *h_A = (float*) calloc(Nrows*Ncolumns, sizeof(float));
  float *h_AT = (float*) calloc(Nrows*Ncolumns, sizeof(float));

  for(int r=0;r<Nrows;++r){
    for(int c=0;c<Ncolumns;++c){
      h_A[r*Ncolumns + c] = c;
    }
  }

  printf("A=[\n");
  for(int r=0;r<Nrows;++r){
    for(int c=0;c<Ncolumns;++c){
      printf("%f ", h_A[r*Ncolumns+c]);
    }
    printf("\n");
  }
  printf("];\n");

  
  float *c_A, *c_AT;
  cudaMalloc(&c_A, Nrows*Ncolumns*sizeof(float));
  cudaMalloc(&c_AT, Nrows*Ncolumns*sizeof(float));

  cudaMemcpy(c_A, h_A, Nrows*Ncolumns*sizeof(float), cudaMemcpyHostToDevice);

  dim3 B(16,16,1);
  dim3 G((Nrows+16-1)/16, (Ncolumns+16-1)/16, 1);
  
  matrixTransposeKernel <<< G , B >>> (Nrows, Ncolumns, c_A, c_AT);
  
  cudaMemcpy(h_AT, c_AT, Nrows*Ncolumns*sizeof(float), cudaMemcpyDeviceToHost);

  printf("AT=[\n");
  for(int r=0;r<Nrows;++r){
    for(int c=0;c<Ncolumns;++c){
      printf("%f ", h_AT[r*Ncolumns+c]);
    }
    printf("\n");
  }
  printf("];\n");

  return 0;
}
