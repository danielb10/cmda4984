#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>

extern "C" {
#include "png_util.h"
}

// to compile

// to convert output png files to an mp4 movie:
// ffmpeg -y -start_number 0 -r 24 -i gol%05d.png -b:v 8192k -c:v mpeg4 gol.mp4

/* function to convert from (i,j) cell index to linear storage index */
__host__ __device__ int idx(int N, int i, int j){
  int n = i + (N + 2)*j;
  return n;  
}




// Use shared memory for accessing "neighbor" memory
__global__ void s_iterate(int N, float*Iold, float *Inew) {
  

  int tx = threadIdx.x;
  int bx = blockIdx.x;
  int dx = blockDim.x;
 
  int ty = threadIdx.y;
  int by = blockIdx.y;
  int dy = blockDim.y; 

  int column = tx + bx*dx;
  int row    = ty + by*dy;
  

  int n;


  // not sure if this is correct but it seemed to help print the board
  // correctly when testing with N = 10
  if (bx == 0 && by ==0) {
      n = idx(N,column,row);
  }
  else if (bx == 0){
      n = idx(N, column, row-2);
  }
  else if (by == 0) {
      n = idx(N, column-2, row);
  }
  else {
    n = idx(N, column-2, row-2);
  }


  __shared__ float s_board[7][7];
  
  if (tx == 0 || ty == 0 || tx == dx-1 || ty == dy-1){
    s_board[tx][ty] = 0;

  }
  if (tx != 0 && ty != 0 && tx != dx-1 && ty != dy-1){
    s_board[tx][ty] = Iold[n];
    
    __syncthreads();
 
    int s = s_board[tx+1][ty] + s_board[tx-1][ty] + s_board[tx][ty-1] 
    + s_board[tx][ty+1] + s_board[tx+1][ty+1] + s_board[tx+1][ty-1] 
    + s_board[tx-1][ty-1] + s_board[tx-1][ty+1];


    int oldState = Iold[n];

    int newState = (oldState==1) ? ( (s==2)||(s==3) ) : (s==3) ;
    
    Inew[n] = newState;
 
  }
}

// Use standard GPU memory
__global__ void iterate(int N, float *Iold, float *Inew) {
 
  int tx = threadIdx.x;
  int bx = blockIdx.x;
  int dx = blockDim.x;
 
  int ty = threadIdx.y;
  int by = blockIdx.y;
  int dy = blockDim.y; 
  
  int column = tx + bx*dx;
  int row    = ty + by*dy;


  int n = idx(N, column, row);
  if (column != 0 && column != N+1 && 
     row != 0 && row != N+1){

     int s = Iold[idx(N, column-1, row-1)] + Iold[idx(N,column+1,row-1)]
       + Iold[idx(N,column,row-1)] + Iold[idx(N,column-1,row)]
       + Iold[idx(N,column+1,row)]+ Iold[idx(N,column,row+1)]
	     + Iold[idx(N,column-1,row+1)] + Iold[idx(N,column+1,row+1)];
      
     int oldState = Iold[n];

     int newState = (oldState==1) ? ( (s==2)||(s==3) ) : (s==3) ;
  
     Inew[n] = newState;
 
  }

}

/* function to print game board for debugging */
void print_board(int N, float *board){
  printf("\n");
  for(int i=1; i<N+1; i=i+1){
    for(int j=1; j<N+1; j=j+1){
      printf("%d", (int)board[idx(N,i,j)]);
    }
    printf("\n");
  }
  printf("\n");
}

/* function to solve for game board using Game of Life rules */
void solve(int N){

  /* Intializes integer random number generator */
  srand(123456);

  int size = (N+2)*(N+2)*sizeof(float);
  // notice the size of these arrays
  float* Inew = (float*) calloc((N+2)*(N+2),sizeof(float));
  float* Iold = (float*) calloc((N+2)*(N+2),sizeof(float));

  for(int i=1;i<N+1;i=i+1){
    for(int j=1;j<N+1;j=j+1){
      // set board state randomly to 1 or 0 
      Iold[idx(N,i,j)] = rand()%2;
      
    }
  }
  /* print initial board*/
  printf("initial game board:");
  print_board(N, Iold);

  /* iterate here */
  int count = 0;   // step counter
  int iostep = 100; // output every iostep
  int output = 1;  // save images if output=1
  int maxsteps = 1000; // maximum number of steps
 

  float* d_Iold;
  float* d_Inew;
  cudaMalloc(&d_Iold, size);
  cudaMalloc(&d_Inew, size);
  cudaMemcpy(d_Inew, Inew, size, cudaMemcpyHostToDevice);
  cudaMemcpy(d_Iold, Iold, size, cudaMemcpyHostToDevice);


  int threads = 5;

  dim3 G(threads+2, threads+2, 1);
  dim3 B((threads+N-1)/threads, (threads+N-1)/threads, 1);

  do{
    /* iterate from Iold to Inew */
    iterate <<<B, G>>> (N, d_Iold, d_Inew);
               
    /* iterate from Inew to Iold */
    iterate <<<B, G>>> (N, d_Inew, d_Iold);
    
    s_iterate<<<B, G>>> (N, d_Iold, d_Inew);

    s_iterate<<<B, G>>> (N, d_Inew, d_Iold);
    
    if(output==1 && count%iostep==0){
      char filename[BUFSIZ];
      FILE *png;
      sprintf(filename, "gol%05d.png", count/iostep);
      png = fopen(filename, "w");
      write_gray_png(png, N+2, N+2, Iold, 0, 1);
      fclose(png);
    }

    cudaMemcpy(Iold, d_Iold, size, cudaMemcpyDeviceToHost);
    cudaMemcpy(Inew, d_Inew, size, cudaMemcpyDeviceToHost);
    
    /* update counter */
    count = count + 1;
  }while(memcmp(Inew, Iold, (N+2)*(N+2)*sizeof(int))!=0 && count <= maxsteps);
  
  /* print out the cell existence in the whole board, then in cell (1 1) and (10 10)*/
  printf("final game board:");
  print_board(N, Iold);
  printf("I_{1 1} = %d\n",   (int)Iold[idx(N,1,1)]);
  printf("I_{10 10} = %d\n", (int)Iold[idx(N,10,10)]);
  printf("Took %d steps\n", count);
  free(Inew);
  free(Iold);
}



/* usage: ./main 100 
         to iterate, solve and display the game board of size N*/
int main(int argc, char **argv){
  // read N from the command line arguments
  int N = atoi(argv[1]);

  // Create timer events
  cudaEvent_t start, end;
  cudaEventCreate(&start);
  cudaEventCreate(&end);


  cudaEventRecord(start);
 
  solve(N);
  
  cudaEventRecord(end);

  float elapsedEvent;
  cudaEventElapsedTime(&elapsedEvent, start, end);

  printf("Elapsed time: %f\n", elapsedEvent);

  return 0;
}
