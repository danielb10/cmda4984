#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "cuda.h"

__global__ void logisticMap(int N, int Nit, float* r, float* x){
  
  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;

  int n = t + b*B;
 
  if(n<N){
    float nr = r[n];
    float nx = x[n];
     
    for (int i=0;i<Nit;i++){
      nx = nr*nx*(1-nx);
    }

    x[n] = nx;
  }
}


int main(int argc, char** argv){


  int Nit = atoi(argv[1]);
  int N = atoi(argv[2]);


  float* h_r = (float*) malloc(N*sizeof(float));
  float* h_x = (float*) malloc(N*sizeof(float));

  float iter = 4.0 / N;

  for (int i=0;i<N;i++){
    h_r[i] = iter*i;
  }
  
  for (int i=0;i<N;i++){
    h_x[i] = drand48();
  }

  float* c_r;
  float* c_x;

  cudaMalloc(&c_r, N*sizeof(float));
  cudaMalloc(&c_x, N*sizeof(float));

  cudaMemcpy(c_r, h_r, N*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(c_x, h_x, N*sizeof(float), cudaMemcpyHostToDevice);

  int B = 256;
  int G = (N+B-1)/B;
  
  logisticMap <<< G, B >>> (N, Nit, c_r, c_x);
  
  cudaMemcpy(h_x, c_x, N*sizeof(float), cudaMemcpyDeviceToHost);

  FILE* f;
  f = fopen("output.txt", "w+");

  if (f == NULL){
    printf("Error creating file");
    exit(1);
  }

  fprintf(f, "r,x\n");
  for(int i=0;i<N;i++){
    fprintf(f, "%g, %g\n", h_r[i], h_x[i]);
  }


}
