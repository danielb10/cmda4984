#include <stdlib.h>
#include <stdio.h>
#include "cuda.h"

/* 
To build:

nvcc -arch=compute_60 -o cudaAddVectorsBroken cudaAddVectorsBroken.cu -lm

To run a test that times using from N=1 to 1025 in steps of 1024

./cudaAddVectorsBroken 1 1024 1025 

*/

/*
 see this stack overflow for a good discussion on error checking via the CUDA API 
 https://stackoverflow.com/questions/14038589/what-is-the-canonical-way-to-check-for-errors-using-the-cuda-runtime-api
*/

inline void cudaError(cudaError_t code, const char *file, int line, bool abort=true){
  if (code != cudaSuccess){
    fprintf(stderr,"cudaErrorAssert: %s %s %d\n", cudaGetErrorString(code), file, line);
    if (abort) exit(code);
  }
}

#define cudaErrorCheck(ans) { cudaError((ans), __FILE__, __LINE__); }

// function to compute linear least squares fit of timing data
void linearLeastSquaresFit(int M, double *B, double *T, double *T0, double *Winf){

  double A00 = M;
  double A01 = 0;
  double A11 = 0;
  double b0  = 0;
  double b1  = 0;
  for(int m=0;m<M;++m){
    A01 += B[m];
    A11 += B[m]*B[m];
    b0  += T[m];
    b1  += B[m]*T[m];
  }
  double A10 = A01;

  double J = A00*A11 - A01*A10;
  double invA00 = A11/J;
  double invA01 = -A01/J;
  double invA10 = -A10/J;
  double invA11 = A00/J;

  *T0   = invA00*b0 + invA01*b1;
  *Winf = invA10*b0 + invA11*b1;
}

__global__ void addKernel(int N, float *c_x, float *c_y, float *c_z){

  // get thread and thread-block indices
  int t = threadIdx.x;

  // switch b and T 
  int b = blockIdx.x;
  int T = blockDim.y;

  // reconstruct linear array index
  int n = t + b*T;

  // Check bounds
  if (n<N){
    c_z[n] = c_x[n]+c_y[n];
  }
}

// HOST main function
int main(int argc, char **argv){

  cudaSetDevice(0); // choose device 0 
  
  if(argc<4){ printf("usage: ./cudaAddvectors Nmin Nskip Nmax\n"); exit(-1); }
  
  // read command line argument
  int Nmin  = atoi(argv[1]);
  int Nskip = atoi(argv[2]);
  int Nmax  = atoi(argv[3]);

  // allocate an array on the HOST
  float *h_x = NULL, *h_y = NULL, *h_z = NULL;
  h_x = (float*) malloc(Nmax*sizeof(float));
  h_y = (float*) malloc(Nmax*sizeof(float));
  
  // change h_y to h_z
  h_z = (float*) malloc(Nmax*sizeof(float));

  // allocate DEVICE arrays
  float *c_x = NULL, *c_y = NULL, *c_z = NULL;

  // change c_y to c_x
  //
  // change Nmin to Nmax
  cudaErrorCheck( cudaMalloc(&c_x, Nmax*sizeof(float)) ); // example using cudaErrorCheck defined above
  cudaMalloc(&c_y, Nmax*sizeof(float));
  cudaMalloc(&c_z, Nmax*sizeof(float));

  // HOST fills array with random numbers
  srand48(123456);
  for(int n=0;n<Nmax;++n){
    h_x[n] = drand48();
    h_y[n] = drand48();
    // change c_z to h_z
    h_z[n] = 0;
  }

  // remove device changing line
  //cudaSetDevice(4); // switch to device 4 

  // copy data to DEVICE
  cudaMemcpy(c_x, h_x, Nmax*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(c_y, h_y, Nmax*sizeof(float), cudaMemcpyHostToDevice);

  { // warm up
    int T = 256;
    int G = (Nmax+T-1)/T;
   
    addKernel <<< G , T >>> (Nmax, c_x, c_y, c_z);
  }
  
  cudaEvent_t start, end;

  cudaEventCreate(&start);
  // change start to end
  cudaEventCreate(&end);

  int cnt = 0;

  for(int N=Nmin;N<=Nmax;N+=Nskip){
    ++cnt;
  }
  double *recordEventDataSize = (double*) calloc(cnt, sizeof(double));
  double *recordEventTimes = (double*) calloc(cnt, sizeof(double));

  cnt = 0;
  for(int N=Nmin;N<=Nmax;N+=Nskip){

    // call kernel to set values in array
    
    // change T to a positive number
    int T = 256;
    int G = (N+T-1)/T;
    
    cudaDeviceSynchronize();
    
    // inserts a dummy event into the GPU queue
    cudaEventRecord(start);

    // queue up kernel
    //
    // change h_z to c_z
    addKernel <<< T, G >>> (N, c_x, c_y, c_z);
    
    // inserts a dummy event into the GPU queue
    //
    // change to end
    cudaEventRecord(end);
    
    cudaDeviceSynchronize();
    
    float elapsedEvent;
    cudaEventElapsedTime(&elapsedEvent, start, end);
    elapsedEvent /= 1000;

    recordEventDataSize[cnt] = N*sizeof(float)*3;
    recordEventTimes[cnt] = elapsedEvent;

    printf("%g, %g; %%%% GB, time(s)\n", recordEventDataSize[cnt]/1.e9, recordEventTimes[cnt]);

    ++cnt;
  }

  double T0, Winf;
  linearLeastSquaresFit(cnt, recordEventDataSize, recordEventTimes, &T0, &Winf);
  Winf = 1./Winf;
  printf("%%%% T0 = %g, Winf = %g (GB/s)\n", T0, Winf/1.e9);
  
  // copy all the data from DEVICE to HOST (blocking)
  cudaMemcpy(h_z, c_z, Nmax*sizeof(float), cudaMemcpyDeviceToHost);

  // change freed arrays
  cudaFree(c_x); cudaFree(c_y); cudaFree(c_z);
  free(h_x); free(h_y); free(h_z);
  
  return 0;
}
